extern crate regex;
use std::io::prelude::*;
use std::net::TcpStream;
use std::rc::Rc;

use buffer::{Buffer, BufferReader};
use messages::{client, server};
use robot::{Direction, Robot};

use std::thread;
use std::time::Duration;

pub mod buffer;
pub mod messages;
pub mod robot;

pub struct Connection {
    stream: Rc<TcpStream>,
    reader: BufferReader,
    robot: Option<robot::Robot>,
}

impl Connection {
    pub fn new(stream: TcpStream) -> Connection {
        let stream = Rc::new(stream);
        let reader = BufferReader::new(Buffer::new(&stream));
        let robot = None;
        Connection {
            stream,
            reader,
            robot,
        }
    }

    pub fn robot(&mut self) -> &mut Robot {
        self.robot.as_mut().unwrap()
    }

    fn send(&mut self, msg: server::Message) -> Result<(), client::Error> {
        let msg = format!("{}{}", msg.encode(), Buffer::delimiter());
        self.stream
            .as_ref()
            .write(&msg.as_bytes())
            .map(|_| ())
            .map_err(|_| client::Error::FatalError)
    }

    pub fn handle_error(&mut self, err: client::Error) {
        let response = match err {
            client::Error::LogicError => server::Message::LogicError,
            client::Error::LoginError => server::Message::LoginFailed,
            client::Error::FatalError => server::Message::SyntaxError,
            _ => {
                return ();
            }
        };
        if let Ok(_) = self.send(response) {}
    }

    fn auth_complete(&mut self) -> Result<(), client::Error> {
        let hash = self.robot().hashed_name();
        self.send(server::Message::Confirmation((hash + 54621) % 65536))
            .and_then(|_| self.reader.read(7))
            .and_then(|message| match message {
                client::Message::Confirmation(hash) => Ok(hash),
                _ => Err(client::Error::FatalError),
            })
            .and_then(|chash| {
                if (chash + (65536 - 45328)) % 65536 == hash {
                    self.send(server::Message::Ok)
                } else {
                    Err(client::Error::LoginError)
                }
            })
    }

    pub fn auth(&mut self) -> Result<(), client::Error> {
        self.reader.read(12).and_then(|message| match message {
            client::Message::Message(username) => {
                self.robot = Some(Robot::new(username));
                self.auth_complete()
            }
            client::Message::Confirmation(username) => {
                self.robot = Some(Robot::new(username.to_string()));
                self.auth_complete()
            }
            _ => Err(client::Error::FatalError),
        })
    }

    fn calc_pos(&mut self, x1: i32, y1: i32) -> Result<(), client::Error> {
        self.move_robot().and_then(|_| {
            let robot::Point { x, y } = self.robot().pos;
            if x == x1 && y == y1 {
                self.calc_pos(x1, y1)
            } else {
                self.robot().set_pos(x1, y1);
                self.robot().set_dir(x - x1, y - y1);
                Ok(())
            }
        })
    }

    pub fn init_pos(&mut self) -> Result<(), client::Error> {
        self.send(server::Message::Move)
            .and_then(|_| self.reader.read(12))
            .and_then(|message| match message {
                client::Message::Ok(x, y) => {
                    self.robot().pos = robot::Point::new(x, y);
                    self.calc_pos(x, y)
                }
                _ => Err(client::Error::FatalError),
            })
    }

    fn set_robot_direction(&mut self, desired_dir: Direction) -> Result<(), client::Error> {
        while self.robot().dir.value() != desired_dir.value() {
            self.turn_robot(server::Message::TurnLeft)?;
        }
        Ok(())
    }

    pub fn get_to_upper_left(&mut self) -> Result<(), client::Error> {
        let desired_dir = if self.robot.as_ref().unwrap().pos.x > -2 {
            Direction::Left
        } else {
            Direction::Right
        };
        self.set_robot_direction(desired_dir)?;
        while self.robot().pos.x != -2 {
            self.move_robot()?;
        }

        let desired_dir = if self.robot.as_ref().unwrap().pos.y < 2 {
            Direction::Up
        } else {
            Direction::Down
        };
        self.set_robot_direction(desired_dir)?;
        while self.robot().pos.y != 2 {
            self.move_robot()?;
        }
        self.set_robot_direction(Direction::Right)?;
        Ok(())
    }

    fn move_robot(&mut self) -> Result<(), client::Error> {
        self.send(server::Message::Move)
            .and_then(|_| self.reader.read(12))
            .and_then(|res| match res {
                client::Message::Ok(x, y) => {
                    if self.robot().pos.x == x && self.robot().pos.y == y {
                        self.move_robot()
                    } else {
                        self.robot().set_pos(x, y);
                        Ok(())
                    }
                }
                _ => Err(client::Error::FatalError),
            })
    }

    fn turn_robot(&mut self, dir: server::Message) -> Result<(), client::Error> {
        match dir {
            server::Message::TurnLeft => self.robot().dir = self.robot().dir.turn_left(),
            server::Message::TurnRight => self.robot().dir = self.robot().dir.turn_right(),
            _ => panic!("Invalid message passed as turn orientation"),
        };

        self.send(dir)
            .and_then(|_| self.reader.read(12))
            .and_then(|res| match res {
                client::Message::Ok(_, _) => Ok(()),
                _ => Err(client::Error::FatalError),
            })
    }

    fn pickup(&mut self) -> Result<Option<String>, client::Error> {
        self.send(server::Message::PickUp)
            .and_then(|_| self.reader.read(100))
            .and_then(|res| match res {
                client::Message::Message(txt) => {
                    if txt.len() > 0 {
                        Ok(Some(txt))
                    } else {
                        Ok(None)
                    }
                }
                client::Message::Confirmation(txt) => Ok(Some(txt.to_string())),
                _ => Err(client::Error::FatalError),
            })
    }

    fn ensure_orientation(&mut self) -> Result<(), client::Error> {
        match self.robot().dir {
            Direction::Left => {
                if self.robot().pos.x == -2 {
                    self.turn_robot(server::Message::TurnLeft)
                } else {
                    Ok(())
                }
            }
            Direction::Right => {
                if self.robot().pos.x == 2 {
                    self.turn_robot(server::Message::TurnRight)
                } else {
                    Ok(())
                }
            }
            Direction::Down => {
                if self.robot().pos.x == -2 {
                    self.turn_robot(server::Message::TurnLeft)
                } else {
                    self.turn_robot(server::Message::TurnRight)
                }
            }
            _ => panic!("Unexpected Up direction."),
        }
    }

    fn find_subroutine(&mut self) -> Result<Option<String>, client::Error> {
        match self.pickup() {
            Ok(res) => {
                if let Some(txt) = res {
                    return Ok(Some(txt));
                }
            }
            Err(err) => {
                return Err(err);
            }
        }
        self.ensure_orientation()
            .and_then(|_| self.move_robot())
            .map(|_| None)
    }

    pub fn find_message(&mut self) -> Result<String, client::Error> {
        while let Ok(res) = self.find_subroutine() {
            if let Some(txt) = res {
                return Ok(txt);
            }
        }
        Err(client::Error::FatalError)
    }

    pub fn logout(&mut self) -> Result<(), client::Error> {
        self.send(server::Message::Logout)
    }
}
