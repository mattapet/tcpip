extern crate regex;
use messages::client;
use regex::Regex;

pub enum Message {
    Recharge,

    Confirmation(u32),
    FullPower,
    Ok(i32, i32),
    Message(String),
}

impl Message {
    fn is_valid_rechargind(msg: &String) -> bool {
        msg == "RECHARGING" && msg.len() <= 10
    }

    fn is_valid_full_power(msg: &String) -> bool {
        msg == "FULL POWER" && msg.len() <= 10
    }

    fn is_valid_ok(msg: &String) -> bool {
        let regex = Regex::new(r"^OK -?\d+ -?\d+$").unwrap();
        regex.is_match(&msg.as_str()) && msg.len() <= 10
    }

    fn is_valid_confirmation(msg: &String) -> bool {
        let regex = Regex::new(r"^\d+$").unwrap();
        regex.is_match(&msg.as_str()) && msg.len() <= 5
    }

    pub fn from(msg: String) -> Result<Self, client::Error> {
        if Self::is_valid_full_power(&msg) {
            Ok(client::Message::FullPower)
        } else if Self::is_valid_ok(&msg) {
            let values: Vec<_> = msg.split_terminator(" ").collect();
            let x: i32 = values[1].parse().unwrap();
            let y: i32 = values[2].parse().unwrap();
            Ok(client::Message::Ok(x, y))
        } else if Self::is_valid_confirmation(&msg) {
            let value: u32 = msg.parse().unwrap();
            Ok(client::Message::Confirmation(value))
        } else if Self::is_valid_rechargind(&msg) {
            Ok(client::Message::Recharge)
        } else if msg.len() <= 98 {
            Ok(client::Message::Message(msg))
        } else {
            Err(client::Error::FatalError)
        }
    }
}

pub enum Error {
    LogicError,
    FatalError,
    LoginError,
    TimeoutError,
}
