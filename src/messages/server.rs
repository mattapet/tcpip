use messages::server;

pub enum Message {
    Move,
    TurnLeft,
    TurnRight,
    PickUp,
    Logout,

    Confirmation(u32),
    Ok,

    LoginFailed,
    SyntaxError,
    LogicError,
}

impl Message {
    pub fn encode(&self) -> String {
        match *self {
            server::Message::Move => "102 MOVE".to_owned(),
            server::Message::TurnLeft => "103 TURN LEFT".to_owned(),
            server::Message::TurnRight => "104 TURN RIGHT".to_owned(),
            server::Message::PickUp => "105 GET MESSAGE".to_owned(),
            server::Message::Logout => "106 LOGOUT".to_owned(),
            server::Message::Confirmation(code) => format!("{}", code),
            server::Message::Ok => "200 OK".to_owned(),
            server::Message::LoginFailed => "300 LOGIN FAILED".to_owned(),
            server::Message::SyntaxError => "301 SYNTAX ERROR".to_owned(),
            server::Message::LogicError => "302 LOGIC ERROR".to_owned(),
        }
    }
}
