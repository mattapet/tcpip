use std::io::prelude::*;
use std::net::TcpStream;
use std::rc::Rc;
use std::time::Duration;

use messages::client;

pub struct BufferReader {
    buff: Buffer,
}

impl BufferReader {
    pub fn new(buff: Buffer) -> BufferReader {
        BufferReader { buff }
    }

    fn recharge(&mut self) -> Result<(), client::Error> {
        self.buff
            .stream
            .as_ref()
            .set_read_timeout(Some(Duration::from_secs(5)))
            .map(|_| ())
            .map_err(|_| client::Error::FatalError)
            .and_then(|_| self.read(12))
            .and_then(|message| match message {
                client::Message::FullPower => Ok(()),
                _ => Err(client::Error::LogicError),
            })
            .and_then(|_| {
                self.buff
                    .stream
                    .as_ref()
                    .set_read_timeout(Some(Duration::from_secs(1)))
                    .map(|_| ())
                    .map_err(|_| client::Error::FatalError)
            })
    }

    pub fn read(&mut self, max_len: usize) -> Result<client::Message, client::Error> {
        use std::cmp::max;
        self.buff
            .read(max(max_len, 12))
            .and_then(|messsage| match messsage {
                client::Message::Recharge => self.recharge().and_then(|_| self.read(max_len)),
                _ => Ok(messsage),
            })
    }
}

pub struct Buffer {
    pub stream: Rc<TcpStream>,
    buff: String,
}

impl Buffer {
    pub fn new(stream: &Rc<TcpStream>) -> Buffer {
        let stream = Rc::clone(stream);
        let buff = String::new();
        stream
            .as_ref()
            .set_read_timeout(Some(Duration::from_secs(1)))
            .unwrap();
        stream
            .as_ref()
            .set_write_timeout(Some(Duration::from_secs(1)))
            .unwrap();
        Buffer { stream, buff }
    }

    pub fn delimiter() -> &'static str {
        "\x07\x08"
    }

    pub fn read(&mut self, max_len: usize) -> Result<client::Message, client::Error> {
        self.ensure_message(max_len).and_then(|_| {
            let msg = self.pop();
            if msg.len() <= max_len - Self::delimiter().len() {
                client::Message::from(msg)
            } else {
                Err(client::Error::FatalError)
            }
        })
    }

    fn read_raw(&mut self) -> Result<(), client::Error> {
        let mut buffer = [0; 128];
        if let Ok(bytes) = self.stream.as_ref().read(&mut buffer) {
            let msg = String::from_utf8_lossy(&buffer[..bytes]);
            self.buff = format!("{}{}", self.buff, msg);
            Ok(())
        } else {
            Err(client::Error::TimeoutError)
        }
    }

    fn ensure_message(&mut self, max_len: usize) -> Result<(), client::Error> {
        while !self.buff.contains("\x07\x08") {
            if self.buff.len() >= max_len {
                return Err(client::Error::FatalError);
            }
            self.read_raw()?;
        }
        Ok(())
    }

    fn pop(&mut self) -> String {
        let msgs: Vec<_> = self.buff
            .split(Self::delimiter())
            .map(|x| String::from(x))
            .collect();
        self.buff = msgs[1..].join(Self::delimiter());
        msgs[0].to_owned()
    }
}
