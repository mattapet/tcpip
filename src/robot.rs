pub enum Direction {
    Up,
    Down,
    Right,
    Left,
}

impl Direction {
    pub fn from(dif_x: i32, dif_y: i32) -> Self {
        match (dif_x, dif_y) {
            (0, 1) => Direction::Up,
            (0, -1) => Direction::Down,
            (-1, 0) => Direction::Left,
            (1, 0) => Direction::Right,
            _ => panic!("Invalid diff of ({}, {})", dif_x, dif_y),
        }
    }

    pub fn value(&self) -> (i32, i32) {
        match *self {
            Direction::Up => (0, 1),
            Direction::Down => (0, -1),
            Direction::Left => (-1, 0),
            Direction::Right => (1, 0),
        }
    }

    pub fn turn_left(&self) -> Self {
        match *self {
            Direction::Up => Direction::Left,
            Direction::Down => Direction::Right,
            Direction::Left => Direction::Down,
            Direction::Right => Direction::Up,
        }
    }

    pub fn turn_right(&self) -> Self {
        match *self {
            Direction::Up => Direction::Right,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
            Direction::Right => Direction::Down,
        }
    }
}

pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }
}

pub struct Robot {
    pub pos: Point,
    pub dir: Direction,
    pub username: String,
}

impl Robot {
    pub fn new(username: String) -> Robot {
        let pos = Point::new(0, 0);
        let dir = Direction::Up;
        Robot { pos, dir, username }
    }

    pub fn set_pos(&mut self, x: i32, y: i32) {
        self.pos = Point::new(x, y);
    }

    pub fn set_dir(&mut self, x: i32, y: i32) {
        self.dir = Direction::from(x, y);
    }

    pub fn hashed_name(&self) -> u32 {
        let sum: u32 = self.username.bytes().map(|x| x as u32).sum();
        sum * 1000 % 65536
    }
}
