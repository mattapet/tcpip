extern crate tcpip;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;

use tcpip::Connection;

fn handle_connection(stream: TcpStream) {
    let mut conn = Connection::new(stream);
    conn.auth()
        .and_then(|_| conn.init_pos())
        .and_then(|_| conn.get_to_upper_left())
        .and_then(|_| conn.find_message())
        .and_then(|message| {
            println!("Secret message is: {}", message);
            conn.logout()
        })
        .map_err(|err| conn.handle_error(err))
        .unwrap_or_default();
}

fn main() {
    let listener = TcpListener::bind("127.0.0.1:3999").unwrap();
    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        thread::spawn(|| handle_connection(stream));
    }
}
